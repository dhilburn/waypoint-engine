﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(WaypointController))]
public class WaypointEditor : Editor
{
    WaypointController controllerScript;

    void Awake()
    {
        controllerScript = (WaypointController)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        
        SerializedProperty controller = serializedObject.FindProperty("waypoints");
        EditorGUILayout.PropertyField(controller);
        if (controller.isExpanded)
        {
            EditorGUILayout.PropertyField(controller.FindPropertyRelative("Array.size"));
            EditorGUI.indentLevel++;
            for (int i = 0; i < controller.arraySize; i++)
            {
                EditorGUILayout.PropertyField(controller.GetArrayElementAtIndex(i));
            }
            EditorGUI.indentLevel--;
        }
        serializedObject.ApplyModifiedProperties();
    }
}