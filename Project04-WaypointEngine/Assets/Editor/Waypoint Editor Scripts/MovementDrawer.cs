﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(MovementScript))]
public class MovementDrawer : PropertyDrawer 
{

    MovementScript thisMovement;
    float extraHeight = 55f;
    float height = 17f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
            SerializedProperty movement = property.FindPropertyRelative("move");
            SerializedProperty movementDelay = property.FindPropertyRelative("moveDelay");
            SerializedProperty moveTime = property.FindPropertyRelative("moveTime");
            SerializedProperty startPoint = property.FindPropertyRelative("startPoint");
            SerializedProperty endPoint = property.FindPropertyRelative("endPoint");
            SerializedProperty curve = property.FindPropertyRelative("curvePoint");

            
            Rect moveTypeDisplay = new Rect(position.x, position.y, position.width, height);
            EditorGUI.PropertyField(moveTypeDisplay, movement);
            float totalHeight = height;
            EditorGUI.indentLevel++;
            switch ((MoveType)movement.enumValueIndex)
            {
                case(MoveType.WAIT):
                    Rect waitDisplay = new Rect(position.x, position.y + height, position.width, height);
                    EditorGUI.PropertyField(waitDisplay, moveTime);
                    break;
                case(MoveType.STRAIGHT_LINE):
                    totalHeight = height;
                    Rect straightMoveStartPointDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(straightMoveStartPointDisplay, startPoint);
                    totalHeight += height;
                    Rect straightMoveEndPointDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(straightMoveEndPointDisplay, endPoint);
                    totalHeight += height;
                    Rect straightDelayDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(straightDelayDisplay, movementDelay);
                    totalHeight += height;
                    Rect straightMoveTimeDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(straightMoveTimeDisplay, moveTime);
                    break;
                case(MoveType.BEZIER_CURVE):
                    totalHeight = height;
                    Rect bezierMoveStartPointDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(bezierMoveStartPointDisplay, startPoint);
                    totalHeight += height;
                    Rect bezierMoveEndPointDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(bezierMoveEndPointDisplay, endPoint);
                    totalHeight += height;
                    Rect bezierMoveCurvePointDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(bezierMoveCurvePointDisplay, curve);
                    totalHeight += height;
                    Rect bezierDelayDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(bezierDelayDisplay, movementDelay);
                    totalHeight += height;
                    Rect bezierMoveTimeDisplay = new Rect(position.x, position.y + totalHeight, position.width, height);
                    EditorGUI.PropertyField(bezierMoveTimeDisplay, moveTime);
                    break;
            }
            EditorGUI.indentLevel--;
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + extraHeight;
    }
}
