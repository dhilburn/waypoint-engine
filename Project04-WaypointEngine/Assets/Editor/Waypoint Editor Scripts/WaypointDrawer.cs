﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(WaypointScript))]
public class WaypointDrawer : PropertyDrawer
{
    WaypointScript thisPoint;
    float height = 17f;
    

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
            SerializedProperty moveInfo = property.FindPropertyRelative("moveInfo");
            SerializedProperty foldout = property.FindPropertyRelative("foldout");

            
            float totalHeight = height;
            switch ((MoveType)property.FindPropertyRelative("moveInfo").FindPropertyRelative("move").enumValueIndex)
            {
                case (MoveType.WAIT):
                    totalHeight = height * 2;
                    break;
                case (MoveType.STRAIGHT_LINE):
                    totalHeight = height * 5;
                    break;
                case (MoveType.BEZIER_CURVE):
                    totalHeight = height * 6;
                    break;
            }
            Rect moveDisplay = new Rect(position.x, position.y, position.width, totalHeight);
            EditorGUI.PropertyField(moveDisplay, moveInfo);
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float extraHeight = height;
        switch((MoveType)property.FindPropertyRelative("moveInfo").FindPropertyRelative("move").enumValueIndex)
        {
            case(MoveType.WAIT):
                extraHeight = height * 2;
                break;
            case(MoveType.STRAIGHT_LINE):
                extraHeight = height * 5;
                break;
            case(MoveType.BEZIER_CURVE):
                extraHeight = height * 6;
                break;
        }
        return base.GetPropertyHeight(property, label) + extraHeight;
    }
}
