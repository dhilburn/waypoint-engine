﻿using UnityEngine;
using System.Collections;

public enum EffectType
{
    CAMERA_SHAKE,
    CAMERA_FADE,
    CAMERA_SPLATTER,
    GRAVITY
}

public enum CameraFadeType
{
    OUT,            // Fade out
    IN,             // Fade in
    OUT_TO_IN,      // Fade out, then in
    IN_TO_OUT       // Fade in, then out
}

public enum GravityDirection
{
    NORMAL,         // Camera angled on ground
    LEFT,           // 90-degree camera angle with ground to the left
    RIGHT,          // 90-degree camera angle with ground to the right
    UP              // Camera upside down
}

[System.Serializable]
public class EffectScript
{
    public EffectType effect;        // Effect to process
    public float effectDelay;        // Time to wait before doing effect
    public float effectDuration;     // How long effect goes on for
    public float shakeIntensity;     // How hard the camera shakes
    public CameraFadeType fadeType;  // How the camera will fade in/out
    public float fadeInTime;         // How long to fade in; used for camera fading and splatters
    public float fadeOutTime;        // How long to fade out; used for camera fading and splatters
    public Color cameraFadeColor;    // Color for camera to fade to
    public Texture splatterImage;    // Image to use for a splatter
    public bool splatterFade;        // flag for if the splatter will fade
    public GravityDirection gravity; // Direction for gravity to act in
    public float toNewGravity;       // How long it takes to rotate to the new gravity
    public bool gravityReturns;      // flag for if the gravity remains in effect or goes back to normal
    public float returnGravityTime;  // How long it takes for gravity to rotate back to normal
}
