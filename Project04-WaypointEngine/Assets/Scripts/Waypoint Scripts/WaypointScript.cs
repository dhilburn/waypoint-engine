﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WaypointScript
{
    public GameObject point;
    public MovementScript moveInfo;
    //public FacingScript facingInfo;
    //public int numEffects;
    //public EffectScript[] effectsInfo;
    public bool foldout;
    //public bool effectsFoldout;
}
