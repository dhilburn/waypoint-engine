﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum FacingType
{
    FREE_LOOK,
    FORCED_LOCATION,
    LOOK_AND_RETURN,
    LOOK_CHAIN
}
[System.Serializable]

public class FacingScript
{
    public FacingType facing;            // Facing follower has at a waypoint
    public int numFocusPoints;           // How many focus points the follower will look at for a look chain
    public float faceDelay;              // Time follower waits before facing
    public float[] toFocusTime;      // Time to focus on target from current facing
    public float[] focusTime;        // Time focused on current target
    public float returnFocusTime;        // Time to return to focus at start of facing
    public GameObject[] focusPoints; // List of points to focus on
}
