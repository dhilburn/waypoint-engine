﻿using UnityEngine;
using System.Collections;

public enum MoveType
{
    WAIT,
    STRAIGHT_LINE,
    BEZIER_CURVE
}
[System.Serializable]
public class MovementScript
{
    [Tooltip("Type of move to make from this waypoint")]
    public MoveType move;           // Movement follower makes at a waypoint
    [Tooltip("Time to wait before moving")]
    public float moveDelay;         // Time follower waits before moving
    [Tooltip("How long it takes to make this move")]
    public float moveTime;          // Time follower takes to move
    [Tooltip("Point to start move from")]
    public GameObject startPoint;   // start point of the move
    [Tooltip("Point to end move at")]
    public GameObject endPoint;     // end point of the move
    [Tooltip("Point bezier curve bends around")]
    public GameObject curvePoint;   // Point to base bezier curve off of
}
